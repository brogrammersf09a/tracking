\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{Work Environment}{4}{0}{2}
\beamer@sectionintoc {3}{Benefits}{6}{0}{3}
\beamer@sectionintoc {4}{Amazon Internship Opportunities in Australia}{8}{0}{4}
\beamer@sectionintoc {5}{Computer Science}{13}{0}{5}
\beamer@sectionintoc {6}{Computational Data}{17}{0}{6}
\beamer@sectionintoc {7}{Software Development}{22}{0}{7}
\beamer@sectionintoc {8}{Information Systems}{24}{0}{8}
